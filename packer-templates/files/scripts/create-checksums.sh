#!/bin/bash
set -e

PACKER_BUILD_NAME="${PACKER_BUILD_NAME}"
NAME="${NAME}"
TIMESTAMP="${TIMESTAMP}"

if ! command -v md5sum &>/dev/null; then
    printf "%s\n" "[WARN] md5sum not found, skipping"
    exit 0
fi

if ! command -v sha512sum &>/dev/null; then
    printf "%s\n" "[WARN] sha512sum not found, skipping"
    exit 0
fi

if test -z "${PACKER_BUILD_NAME}" || test -z "${NAME}" || test -z "${TIMESTAMP}"; then
    printf "%s\n" "[WARN] \$PACKER_BUILD_NAME|\$NAME|\$TIMESTAMP not supplied for checksum creation, skipping"
    exit 0
fi

OUTPUT_DIR="output/${PACKER_BUILD_NAME}"
mkdir -p "${OUTPUT_DIR}" &>/dev/null

if test "${PACKER_BUILD_NAME}" == "qemu"; then
    FILENAME="${PACKER_BUILD_NAME}-${NAME}_${TIMESTAMP}.qcow2"
    mv "${PACKER_BUILD_NAME}_output_${NAME}/${PACKER_BUILD_NAME}-${NAME}_${TIMESTAMP}" "${OUTPUT_DIR}/${FILENAME}"
else
    FILENAME="${PACKER_BUILD_NAME}-${NAME}_${TIMESTAMP}.ova"
    mv "${PACKER_BUILD_NAME}_output_${NAME}/${PACKER_BUILD_NAME}-${NAME}_${TIMESTAMP}.ova" "${OUTPUT_DIR}/${FILENAME}"
fi
rm -rf "${PACKER_BUILD_NAME}_output_${NAME}"

(
    cd "${OUTPUT_DIR}"
    md5sum "${FILENAME}" > "${FILENAME}.sums"
    sha512sum "${FILENAME}" >> "${FILENAME}.sums"
)

printf "%s\n" "[INFO] created md5 and sha512 checksums for ${OUTPUT_DIR}/${FILENAME}"
