[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

try {
    $url = "https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/latest-virtio/virtio-win-guest-tools.exe"
    (New-Object System.Net.WebClient).DownloadFile($url, "C:\virtio-win-guest-tools.exe")
    Write-Output "[INFO] Downloaded $url"

    Start-Process -FilePath "C:\virtio-win-guest-tools.exe" -ArgumentList "/install /passive /norestart" -Wait -Verbose
    Write-Output "[INFO] Successfully installed VirtIO Guest Tools"
} catch {
    Write-Host "[ERR] Error occured while installing VirtIO Guest Tools"
    Write-Host "$($_.Exception.Message)"
}
