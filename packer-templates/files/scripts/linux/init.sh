#!/bin/bash
set -e
pushd "$(dirname ${BASH_SOURCE:0})" &>/dev/null
trap 'popd &>/dev/null' EXIT

export DEBIAN_FRONTEND=noninteractive

apt-get update
apt-get install -y qemu-guest-agent
apt-get clean
apt-get autoclean
