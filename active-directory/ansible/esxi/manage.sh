#!/bin/bash

if ! test -f .env; then
    printf "%s\n" "[ERR] .env file not found"
    exit 1
fi

case $1 in
    start)
        source .env
        ansible-playbook -i contoso.com_inventory.ini -e "esxi_hostname=${esxi_hostname} esxi_username=${esxi_username} esxi_password=${esxi_password}" /dev/stdin << EOF
- name: power on inventory
  hosts: all
  gather_facts: no
  tasks:
    - name: Set the state of a virtual machine to poweroff
      community.vmware.vmware_guest_powerstate:
        hostname: "{{ esxi_hostname }}"
        username: "{{ esxi_username }}"
        password: "{{ esxi_password }}"
        validate_certs: false
        folder: "/ha-datacenter/vm"
        name: "{{ inventory_hostname }}"
        state: powered-on
      delegate_to: localhost
EOF
        ;;

    stop)
        source .env
        ansible-playbook -i contoso.com_inventory.ini -e "esxi_hostname=${esxi_hostname} esxi_username=${esxi_username} esxi_password=${esxi_password}" /dev/stdin << EOF
- name: power off inventory
  hosts: all
  gather_facts: no
  tasks:
    - name: Set the state of a virtual machine to poweroff
      community.vmware.vmware_guest_powerstate:
        hostname: "{{ esxi_hostname }}"
        username: "{{ esxi_username }}"
        password: "{{ esxi_password }}"
        validate_certs: false
        folder: "/ha-datacenter/vm"
        name: "{{ inventory_hostname }}"
        state: shutdown-guest
      delegate_to: localhost
EOF

        ;;
    *) printf "%s\n" "usage: $(basename $0) start|stop"; exit 1;;
esac
