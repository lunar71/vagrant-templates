#!/bin/bash

if command -v pip &>/dev/null; then
    pip install ansible requests PyVmomi pywinrm paramiko
else
    printf "%s\n" "[ERR] pip not found"
    exit 1
fi

if command -v ansible-galaxy &>/dev/null; then
    ansible-galaxy collection install community.vmware --force
else
    printf "%s\n" "[ERR] ansible/ansible-galaxy not found"
    exit 1
fi

