param (
    [string]$DomainName = "contoso.com",
    [string]$Username   = "Administrator",
    [string]$Password   = "packer"
)

Import-Module ADCSTemplate

Get-ChildItem -Path "C:\setup\templates" -Filter *.json | % {
    $TemplateName = $_.BaseName
    if (-not(Get-ADCSTemplate -DisplayName $TemplateName)) {
        New-ADCSTemplate `
            -DisplayName $TemplateName `
            -JSON (Get-Content "C:\setup\templates\$_" -Raw) `
            -Identity "$DomainName\Domain Users" `
            -Publish
    }
}
