param (
    [string]$DomainName = "contoso.com",
    [string]$Username   = "Administrator",
    [string]$Password   = "packer",

    [Parameter(Mandatory=$true)]
    [string]$Script,

    [Parameter(Mandatory=$true)]
    [string[]]$Arguments
)

$securePassword = ConvertTo-SecureString $Password -AsPlainText -Force
$credential = New-Object System.Management.Automation.PSCredential("${DomainName}\${Username}", $securePassword)

try {
    $session = New-PSSession -Credential $credential -Authentication Default
    Invoke-Command -Session $session -ScriptBlock {
        param ($Script, $Arguments)
        $argString = $Arguments -join ' '
        Start-Process -FilePath "powershell.exe" -ArgumentList "-ExecutionPolicy Bypass -File $Script $argString" -Wait -NoNewWindow
    } -ArgumentList $Script, $Arguments -ErrorAction Stop

    Write-Host "[INFO] Script executed successfully."
    Remove-PSSession $session
} catch {
    Write-Host "[ERR] Failed to execute script. Error: $_"
    if ($session) {
        Remove-PSSession $session
    }
}

