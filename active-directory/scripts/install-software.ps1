Set-MpPreference -DisableRealtimeMonitoring $true -ErrorAction SilentlyContinue | Out-Null
Start-Process -FilePath "C:\software\Sysmon64.exe" -ArgumentList "-accepteula -i C:\software\sysmonconfig-export.xml" -Wait -Verbose
Start-Process -FilePath "C:\Windows\System32\MsiExec.exe" -ArgumentList "/i C:\software\googlechromestandaloneenterprise64.msi /qb" -Wait -Verbose
Start-Process -FilePath "C:\software\npp.exe" -ArgumentList "/S" -Wait -Verbose
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run", "bginfo", "C:\BgInfo\Bginfo.exe C:\BgInfo\BgInfo.bgi /timer:00 /nolicprompt /silent")
