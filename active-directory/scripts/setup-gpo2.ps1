param (
    [string]$DomainName = "contoso.com"
)

$DomainNameDN = "DC=$($DomainName.Split(".")[0]),DC=$($DomainName.Split(".")[1])"
$DomainUsers = Get-ADGroup "Domain Users"
try {
    $GPO1 = New-GPO -Name "TestGPO1"
    $GPO2 = New-GPO -Name "TestGPO2"
    Set-GPPermission -Name $GPO1.DisplayName -PermissionLevel GpoEditDeleteModifySecurity -TargetName $DomainUsers.Name -TargetType Group | Out-Null
    Set-GPPermission -Name $GPO2.DisplayName -PermissionLevel GpoEditDeleteModifySecurity -TargetName $DomainUsers.Name -TargetType Group | Out-Null

    Write-Host "[INFO] Created insecure GPOs $($GPO1.DisplayName), $($GPO2.DisplayName) with GpoEditDeleteModifySecurity"
} catch {
    Write-Host "[ERR] Failed to create insecure GPOs $($GPO1.DisplayName), $($GPO2.DisplayName) with GpoEditDeleteModifySecurity"
}

try {
    New-GPLink -Name $GPO1.DisplayName -Target "$DomainNameDN" -LinkEnabled Yes | Out-Null
    New-GPLink -Name $GPO2.DisplayName -Target "$DomainNameDN" -LinkEnabled Yes | Out-Null
    
    Write-Host "[INFO] Created GP links for $($GPO1.DisplayName), $($GPO2.DisplayName) on $DomainNameDN"
} catch {
    Write-Host "[ERR] Failed to create GP links for $($GPO1.DisplayName), $($GPO2.DisplayName) on $DomainNameDN"
}
